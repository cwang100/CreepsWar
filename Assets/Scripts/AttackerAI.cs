﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerAI : GeneralAI {
    public override float CalculateAttackScore()
    {
        float Attack = 0;
        foreach (CharacterBehavior target in enemies)
        {
            if (target.dead)
            {
                continue;
            }
            if (GetDistance(gameObject, target.gameObject) <= attackDistance)
                Attack += 4;
            else
            {
                Attack += 2;
            }
        }
        return Attack;
    }

    public override float CalculateHideScore()
    {
        float Hide = 0;
        if (enemies.Count > 0 && self.hp < self.maxHp * 0.6f)
        {
            Hide += (maxHideCount - hideCount) * enemies.Count;
        }
        foreach (CharacterBehavior target in enemies)
        {
            if (target.dead)
            {
                continue;
            }
            if (target.aimTarget == self)
                Hide += (maxHideCount - hideCount);
        }
        return Hide;
    }

    public override float CalculateRunScore()
    {
        float Run = 0;

        if (ChooseTarget() == null)
            Run = 200;
        return Run;
    }

    public override CharacterBehavior ChooseTarget()
    {
        CharacterBehavior closest = null;
        foreach (CharacterBehavior enemy in enemies)
        {
            if (enemy.dead)
                continue;
            if (closest == null || (GetDistance(gameObject, enemy.gameObject) < GetDistance(gameObject, closest.gameObject) && CastRay(enemy)))
            {
                closest = enemy;
            }
        }

        foreach (CharacterBehavior friend in friends)
        {
            if (friend.dead)
            {
                continue;
            }
            if (friend.aimTarget != null && !friend.aimTarget.dead && closest == null)
            {
                closest = friend.aimTarget.GetComponent<CharacterBehavior>();
            }
        }
        return closest;
    }

    public override void Attack(CharacterBehavior closest)
    {
        self.aimTarget = closest;
        self.hiding = false;
        self.hideAttacking = false;
        if (GetDistance(gameObject, closest.gameObject) <= attackDistance && CastRay(closest))
        {
            self.attacking = true;
            self.moveTarget = self.transform.position;
            DealDamage(closest);
        }
        else
        {
            self.attacking = false;
            self.moveTarget = closest.transform.position;
        }
    }

    public override void Run(CharacterBehavior closest)
    {
        self.aimTarget = null;
        self.attacking = false;
        self.hiding = false;
        self.hideAttacking = false;
        if (closest)
        {
            self.moveTarget = closest.transform.position;
        }
        else
        {
            self.moveTarget = new Vector3((Random.value - 0.5f) * 50f, 0, (Random.value - 0.5f) * 50f) + transform.position;
        }
    }

    public override void Hide(CharacterBehavior closest)
    {
        if (closest == null)
        {
            Run(null);
            return;
        }
        if (self.hiding)
        {
            self.moveTarget = transform.position;
            HideAttack(closest);
            return;
        }
        ObstacleDemo hideTarget = null ;
        foreach (ObstacleDemo obstacle in obstacles)
        {
            if (!obstacle.Occupied && (hideTarget == null || GetDistance(this.gameObject, obstacle.gameObject) < GetDistance(this.gameObject, hideTarget.gameObject)))
            {
                hideTarget = obstacle;
            }
        }
        if (hideTarget)
        {
            Transform hidePos = null;
            float maxDistance = 0;
            foreach (Transform pos in hideTarget.HidePosition)
            {
                float distance = 0;
                foreach (CharacterBehavior enemy in enemies)
                {
                    distance += GetDistance(pos.gameObject, enemy.gameObject);
                }

                if (hidePos == null || distance > maxDistance)
                {
                    hidePos = pos;
                    maxDistance = distance;
                }
            }
            self.moveTarget = hidePos.position;
            self.attacking = false;
            if (!self.CheckMovingStatus())
            {
                HideAttack(closest);
            }
            else
            {
                self.hiding = false;
                self.hideAttacking = false;
            }
        }
        else
        {
            Attack(closest);
        }
    }

    public override void HideAttack(CharacterBehavior closest)
    {
        self.hiding = true;
        hideCount++;
        if (GetDistance(gameObject, closest.gameObject) <= attackDistance && CastRay(closest) && !closest.dead)
        {
            self.aimTarget = closest;
            self.hideAttacking = true;
            DealDamage(closest);
        }
        else
        {
            self.hideAttacking = false;
        }
    }

    public override void DealDamage(CharacterBehavior target)
    {
        if (!target.hiding)
        {
            if (Random.Range(0, 5) < 3)
            {
                target.LoseHP(1);
            }
        }
        else
        {
            if (Random.Range(0, 7) < 3)
            {
                target.LoseHP(1);
            }
        }
    }
}
