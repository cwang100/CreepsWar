﻿using DragonBones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterAnimator : MonoBehaviour {
    public GameObject front;
    public GameObject back;
    public UnityArmatureComponent frontComponent;
    public UnityArmatureComponent backComponent;
    public UnityArmatureComponent currentComponent;
    public GameObject attackParticle;
    public GameObject aimParticle;
    public Slider hpSlider;

    private AnimationType currentAnimName;
    private bool facingPositive;
    private Bone _aimPoint;
    private DragonBones.AnimationState _aimState;
    private bool blockTransform;
    private bool blockOtherAnim;

    public enum AnimationType
    {
        none,
        idle,
        running,
        shoot,
        runfast,
        throwover,
        reload,
        LieDown,
        LieDownIdle,
        Die,
        injured,
        aim
    }

    private void Start()
    {
        ShowFront();
        _aimPoint = currentComponent.armature.GetBone("右手");
        _aimState = frontComponent.animation.FadeIn("turn", 0.1f, 1, 0, "aimGroup");
        _aimState.resetToPose = false;
        _aimState.Stop();
        _aimState.currentTime = 2.8f;
        FacePositive();
        PlayAnim(AnimationType.idle);
    }

    public void ShowFront()
    {
        if ((!front.activeSelf && currentAnimName != AnimationType.throwover) || currentComponent == null)
        {
            front.SetActive(true);
            back.SetActive(false);
            currentComponent = frontComponent;
            ContinueAnim();
        }
    }

    public void ShowBack()
    {
        if ((!back.activeSelf && (currentAnimName == AnimationType.idle || currentAnimName == AnimationType.running)) && !blockTransform || currentComponent == null)
        {
            front.SetActive(false);
            back.SetActive(true);
            currentComponent = backComponent;
            ContinueAnim();
        }
    }

    public void PlayAnim(AnimationType name)
    {
        if (currentAnimName != name && !blockOtherAnim)
        {
            if (name != AnimationType.idle && name != AnimationType.running)
            {
                ShowFront();
            }
            if (name == AnimationType.LieDown && currentAnimName == AnimationType.LieDownIdle || currentAnimName == AnimationType.Die)
            {
                return;
            }
            else if (name == AnimationType.Die)
            {
                currentComponent.animation.FadeIn(name.ToString(), 0, 1);
                currentAnimName = name;
            }
            else if (name == AnimationType.aim)
            {
                currentComponent.animation.FadeIn(AnimationType.idle.ToString());
                currentAnimName = name;
            }
            else
            {
                currentComponent.animation.FadeIn(name.ToString());
                currentAnimName = name;
            }
            
            attackParticle.SetActive(name == AnimationType.shoot);
            if (aimParticle)
            {
                aimParticle.SetActive(name == AnimationType.aim);
            }
            if (name == AnimationType.throwover)
            {
                blockOtherAnim = true;
                StartCoroutine(Throwing());
            }
            if (name == AnimationType.LieDown)
            {
                blockOtherAnim = true;
                StartCoroutine(LieDown());
            }
            if (name == AnimationType.injured)
            {
                blockOtherAnim = true;
                StartCoroutine(Injured());
            }
        }
    }

    public void AimTarget(Vector3 point)
    {
        blockTransform = true;
        ShowFront();
        float aimOffsetY = this._aimPoint.global.y * this.transform.localScale.y;
        Vector3 aimPoint = Camera.main.WorldToScreenPoint(point);
        Vector3 charactorPoint = Camera.main.WorldToScreenPoint(currentComponent.transform.position + new Vector3(0, aimOffsetY, 0));

        facingPositive = aimPoint.x > charactorPoint.x ? true : false;
        currentComponent.armature.flipX = facingPositive;
        float _aimRadian;
        if (facingPositive)
        {
            _aimRadian = Mathf.Atan2(-(aimPoint.y - charactorPoint.y), aimPoint.x - charactorPoint.x);
        }
        else
        {
            _aimRadian = Mathf.PI - Mathf.Atan2(-(aimPoint.y - charactorPoint.y), aimPoint.x - charactorPoint.x);
            if (_aimRadian > Mathf.PI)
            {
                _aimRadian -= Mathf.PI * 2.0f;
            }
        }

        // Calculate progress
        float progress = Mathf.Abs((_aimRadian + Mathf.PI / 2) / Mathf.PI);
        // Set currentTime

        float currentTime = progress * (this._aimState.totalTime);

        this._aimState.currentTime = Mathf.Min(currentTime, _aimState.totalTime);
    }

    public void AimDone()
    {
        blockTransform = false;
        if (_aimState != null)
            _aimState.currentTime = 2.8f;
    }

    private IEnumerator Throwing()
    {
        float time = 0f;
        while (time < 0.5f)
        {
            time += Time.deltaTime;
            currentComponent.transform.localPosition += new Vector3(0, Time.deltaTime * 7, 0);
            yield return null;
        }
        while (0.5f <= time && time < 1f)
        {
            time += Time.deltaTime;
            currentComponent.transform.localPosition -= new Vector3(0, Time.deltaTime * 7, 0);
            yield return null;
        }

        currentComponent.transform.localPosition = Vector3.zero;
        currentAnimName = AnimationType.none;
        blockOtherAnim = false;
        PlayAnim(AnimationType.idle);
        yield return null;
    }

    private IEnumerator LieDown()
    {
        float time = 0f;
        while (time < 1f)
        {
            time += Time.deltaTime;
            yield return null;
        }
        blockOtherAnim = false;
        currentComponent.animation.FadeIn(AnimationType.LieDownIdle.ToString(), 0, 1);
        currentAnimName = AnimationType.LieDownIdle;
        yield return null;
    }

    private IEnumerator Injured()
    {
        float time = 0f;
        while (time < 0.4f)
        {
            time += Time.deltaTime;
            yield return null;
        }
        blockOtherAnim = false;
        yield return null;
    }

    public void FacePositive()
    {
        if (!facingPositive && !blockTransform)
        {
            facingPositive = true;
            currentComponent.armature.flipX = true;
        }
    }

    public void FaceNegative()
    {
        if (facingPositive && !blockTransform)
        {
            facingPositive = false;
            currentComponent.armature.flipX = false;
        }
    }

    public void ContinueAnim()
    {
        if (currentAnimName != AnimationType.throwover)
        {
            currentComponent.animation.FadeIn(currentAnimName.ToString());
            currentComponent.armature.flipX = facingPositive;
        }
    }
}
