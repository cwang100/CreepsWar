﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDemo : MonoBehaviour {
    public GameObject shooterObject;
    public GameObject sniperObject;

    public Transform shooterInitPos;
    public Transform enemyInitPos;

    private GameObject shooter;
    private CharacterBehavior shooterBehavior;
    private GameObject enemy;
    private CharacterBehavior enemyBehavior;

    public float shooterCount;
    public float sniperCount;
    public float enemyshooterCount;
    public float enemysniperCount;
    private float time;
    // Use this for initialization
    void Start () {
        for (int i = 0; i < shooterCount; i ++ )
        {
            shooter = Instantiate(shooterObject, shooterInitPos.position, Quaternion.identity);
        }
        for (int i = 0; i < sniperCount; i++)
        {
            shooter = Instantiate(sniperObject, shooterInitPos.position, Quaternion.identity);
        }
        for (int i = 0; i < enemyshooterCount; i++)
        {
            enemy = Instantiate(shooterObject, enemyInitPos.position, Quaternion.identity);
            enemyBehavior = enemy.GetComponent<CharacterBehavior>();

            enemyBehavior.isEnemy = true;
        }
        for (int i = 0; i < enemysniperCount; i++)
        {
            enemy = Instantiate(sniperObject, enemyInitPos.position, Quaternion.identity);
            enemyBehavior = enemy.GetComponent<CharacterBehavior>();

            enemyBehavior.isEnemy = true;
        }
    }
}
