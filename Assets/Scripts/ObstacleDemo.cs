﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDemo : MonoBehaviour {
    public Transform[] HidePosition;
    public bool Occupied;

    private float time;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if (time > 0.5f)
        {
            time -= 0.5f;
            Collider[] collides = Physics.OverlapSphere(transform.position, 14);
            Occupied = false;
            foreach (Collider col in collides)
            {
                CharacterBehavior otherCharacter = col.gameObject.GetComponent<CharacterBehavior>();
                if (otherCharacter != null && otherCharacter.hiding == true)
                {
                    Occupied = true;
                }
            }
        }
    }
}
