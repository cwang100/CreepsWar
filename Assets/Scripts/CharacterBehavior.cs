﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class CharacterBehavior : MonoBehaviour {
    public GameObject characterPrefab;
    
    public CharacterBehavior aimTarget;
    public Vector3 moveTarget;
    public bool attacking;
    public bool aiming;
    public bool hiding;
    public bool hideAttacking;
    public bool dead = false;
    public bool isEnemy = false;
    public float maxHp = 5;
    public float hp = 5;


    private CharacterAnimator animator;
    private GameObject character;
    private float xSpeed;
    private float zSpeed;
    private bool moving;
    private NavMeshAgent agent;

    // Use this for initialization
    void Start () {
        character = GameObject.Instantiate(characterPrefab, transform.position, Quaternion.identity);

        animator = character.GetComponent<CharacterAnimator>();
        moveTarget = this.transform.position;
        agent = GetComponent<NavMeshAgent>();
        attacking = aiming = hiding = hideAttacking = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (!dead)
        {
            ChangeAgentStatus();
            agent.SetDestination(moveTarget);
            character.transform.position = this.transform.position;
            CalculateSpeed();
        }
        else
        {
            agent.radius = 0.01f;
        }
        ChangeAnim();
        animator.hpSlider.value = hp / 5f;
    }

    public bool CheckMovingStatus()
    {
        if (Vector3.Distance(moveTarget, transform.position) < 3f || agent.isPathStale)
        {
            return false;
        }
        return true;
    }

    private void ChangeAgentStatus()
    {
        if (attacking || hiding || aimTarget != null)
        {
            agent.areaMask = 3;
            agent.autoTraverseOffMeshLink = false;
            agent.CompleteOffMeshLink();

        }
        else
        {
            agent.areaMask = -1;
            agent.autoTraverseOffMeshLink = true;
        }
    }

    public void LoseHP(float amount)
    {
        hp -= amount;
        animator.PlayAnim(CharacterAnimator.AnimationType.injured);
        if (hp <= 0)
        {
            dead = true;
            attacking = false;
            hiding = false;
        }
    }

    private void ChangeAnim()
    {
        if (!animator)
            return;
        if (dead == true)
        {
            animator.PlayAnim(CharacterAnimator.AnimationType.Die);
            return;
        }
        if (attacking)
        {
            animator.AimTarget(aimTarget.transform.position);
            animator.PlayAnim(CharacterAnimator.AnimationType.shoot);
        }
        else if (aiming)
        {
            animator.AimTarget(aimTarget.transform.position);
            animator.PlayAnim(CharacterAnimator.AnimationType.aim);
        }
        else if (hiding)
        {
            animator.PlayAnim(CharacterAnimator.AnimationType.LieDown);
            if (hideAttacking)
            {
                animator.AimTarget(aimTarget.transform.position);
                animator.attackParticle.SetActive(true);
            }
            else
            {
                animator.attackParticle.SetActive(false);
            }
        }
        else if (agent.isOnOffMeshLink && agent.autoTraverseOffMeshLink)
        {
            animator.PlayAnim(CharacterAnimator.AnimationType.throwover);
        }
        else if (!attacking && !hiding)
        {
            if (xSpeed != 0 || zSpeed != 0)
                animator.PlayAnim(CharacterAnimator.AnimationType.running);
            else
                animator.PlayAnim(CharacterAnimator.AnimationType.idle);
            if (aimTarget)
                animator.AimTarget(aimTarget.transform.position);
            else
                animator.AimDone();
        }

        if (zSpeed < 0)
        {
            animator.ShowFront();
        }
        else if (zSpeed > 0)
        {
            animator.ShowBack();
        }
        if (xSpeed < 0)
        {
            animator.FaceNegative();
        }
        else if (xSpeed > 0)
        {
            animator.FacePositive();
        }
    }

    private void CalculateSpeed()
    {
        if (agent.velocity.magnitude < 0.1f || agent.isPathStale)
        {
            xSpeed = zSpeed = 0;
            return;
        }
        xSpeed = agent.velocity.x;
        zSpeed = agent.velocity.z;
    }
}
