﻿using DragonBones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo : MonoBehaviour {
    public GameObject shooterObject;

    public GameObject target;
    private GameObject shooter;
    private CharacterAnimator character;
    private float xAxis;
    private float zAxis;
	// Use this for initialization
	void Start () {
        shooter = GameObject.Instantiate(shooterObject);
        character = shooter.GetComponent<CharacterAnimator>();
    }
	
	// Update is called once per frame
	void Update () {
        character.AimTarget(target.transform.position);

        if (Input.GetKey(KeyCode.W))
        {
            zAxis = 0.4f;
        }
        if(Input.GetKey(KeyCode.S))
        {
            zAxis = -0.4f;
        }
        if(Input.GetKey(KeyCode.A))
        {
            xAxis = -0.4f;
        }
        if (Input.GetKey(KeyCode.D))
        {
            xAxis = 0.4f;
        }
        if (!Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A))
        {
            xAxis = 0;
        }
        if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S))
        {
            zAxis = 0;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            xAxis = zAxis = 0;
            character.PlayAnim(CharacterAnimator.AnimationType.shoot);
        } else if (Input.GetKey(KeyCode.Z))
        {
            character.PlayAnim(CharacterAnimator.AnimationType.runfast);
        }
        else if (Input.GetKey(KeyCode.X))
        {
            character.PlayAnim(CharacterAnimator.AnimationType.throwover);
        }
        else if (xAxis == 0 && zAxis == 0)
        {
            character.PlayAnim(CharacterAnimator.AnimationType.idle);
        } else
        {
            character.PlayAnim(CharacterAnimator.AnimationType.running);
        }

        shooter.transform.Translate(new Vector3(xAxis, 0, zAxis));
        if (zAxis < 0)
        {
            character.ShowFront();
        }
        else if (zAxis > 0)
        {
            character.ShowBack();
        }
        if (xAxis < 0)
        {
            character.FaceNegative();
        }
        else if (xAxis > 0)
        {
            character.FacePositive();
        }
    }
}
