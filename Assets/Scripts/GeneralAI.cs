﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralAI : MonoBehaviour {
    public SphereCollider vision;
    public List<CharacterBehavior> enemies;
    public List<CharacterBehavior> friends;
    public List<ObstacleDemo> obstacles;
    public CharacterBehavior self;
    public float AttackScore;
    public float MoveScore;
    public float HideScore;
    public float attackDistance = 35f;
    public float recalculateTime = 1f;
    public float maxHideCount = 15;
    public float hideCount = 0;

    private float time;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time > recalculateTime)
        {
            time -= recalculateTime;
            if (self.dead)
            {
                self.moveTarget = transform.position;
                return;
            }
            DetectCollide();
            HideScore= CalculateHideScore();
            AttackScore = CalculateAttackScore();
            MoveScore = CalculateRunScore();
            CharacterBehavior closest = ChooseTarget();


            if (AttackScore > MoveScore && AttackScore > HideScore)
            {
                Attack(closest);
            }
            else if (HideScore > AttackScore && HideScore > MoveScore)
            {
                Hide(closest);
            }
            else
            {
                ResetHide();
                Run(closest);
            }
        }
    }

    public virtual float CalculateAttackScore()
    {
        return 0;
    }

    public virtual float CalculateHideScore()
    {
        return 0;
    }

    public virtual float CalculateRunScore()
    {
        return 0;
    }

    public virtual CharacterBehavior ChooseTarget()
    {
        return null;
    }

    public bool CastRay(CharacterBehavior other)
    {
        RaycastHit hit;
        Vector3 target = other.transform.position;
        if (Physics.Raycast(transform.position, Vector3.Normalize(target - transform.position), out hit))
        {
            //Debug.DrawRay(transform.position, other.transform.position - transform.position, self.isEnemy? Color.green : Color.blue, 0.3f);
            if (hit.transform.position != target)
                return false;
            Debug.DrawRay(transform.position, other.transform.position - transform.position, self.isEnemy ? Color.red : Color.black, 0.3f);
        }

        return true;
    }

    public virtual void Attack(CharacterBehavior closest)
    {

    }

    public virtual void Run(CharacterBehavior closest)
    {

    }

    public virtual void Hide(CharacterBehavior closest)
    {

    }

    public virtual void HideAttack(CharacterBehavior closest)
    {

    }

    public virtual void DealDamage(CharacterBehavior target)
    {

    }

    public void DetectCollide()
    {
        Collider[] collides = Physics.OverlapSphere(transform.position, vision.radius);
        foreach (Collider col in collides)
        {
            if (col.gameObject.CompareTag("Obstacle") && !obstacles.Contains(col.gameObject.GetComponent<ObstacleDemo>()))
            {
                obstacles.Add(col.gameObject.GetComponent<ObstacleDemo>());
            }
            else
            {
                CharacterBehavior otherCharacter = col.gameObject.GetComponent<CharacterBehavior>();
                if (otherCharacter != null && !otherCharacter.dead)
                {
                    if (otherCharacter.isEnemy != self.isEnemy && !enemies.Contains(otherCharacter))
                    {
                        enemies.Add(otherCharacter);
                    }
                    else if (otherCharacter.isEnemy == self.isEnemy && !friends.Contains(otherCharacter))
                    {
                        friends.Add(otherCharacter);
                    }
                }
            }
        }
    }

    public float GetDistance(GameObject a, GameObject b)
    {
        return Mathf.Abs(Vector3.Distance(a.transform.position, b.transform.position));
    }

    private void ResetHide()
    {
        maxHideCount -= 1;
        hideCount = 0;
    }
}
